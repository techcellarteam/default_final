<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// ------------------------------------------------------------------------

/*
 * CodeIgniter URL Helpers Extension
 *
 * @category	Helpers
 * @author	-_-
 */

define('_ASSETS_DIR_', config_item('base_url') . 'assets/');
define('_AVATARS_DIR_', _ASSETS_DIR_ . 'avatars/');
define('_CSS_DIR_', _ASSETS_DIR_ . 'css/');
define('_FONT_DIR_', _ASSETS_DIR_ . 'font/');
define('_IMAGES_DIR_', _ASSETS_DIR_ . 'images/');
define('_IMG_DIR_', _ASSETS_DIR_ . 'img/');
define('_JS_DIR_', _ASSETS_DIR_ . 'js/');

define('_UPLOAD_DIR_', config_item('base_url') . 'upload/');
define('_UPLOAD_IMAGES_DIR_', _UPLOAD_DIR_ . 'images/');
define('_UPLOAD_USER_DIR_', _UPLOAD_DIR_ . 'user/');
define('_UPLOAD_COMPANY_DIR_', _UPLOAD_DIR_ . 'company/');
define('_UPLOAD_SURVEY_DIR_', _UPLOAD_DIR_ . 'survey/');

define('_SYSTEM_URL_', config_item('base_url') . 'system/');
define('_USER_URL_', config_item('base_url') . 'users/');
define('_DEPARTMENTS_URL_', config_item('base_url') . 'departments/');
define('_USERTYPES_URL_', config_item('base_url') . 'user_types/');

/*
 * Admin
 */
if (!function_exists('admin_url')) {

    function system_url($subdirectory = '') {
        $link = _SYSTEM_URL_;
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('system_dir')) {

    function system_dir($subdirectory = '') {
        $link = 'system/';
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('users_url')) {

    function users_url($subdirectory = '') {
        $link = _USER_URL_;
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}
if (!function_exists('users_dir')) {

    function users_dir($subdirectory = '') {
        $link = 'users/';
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('departments_url')) {

    function departments_url($subdirectory = '') {
        $link = _DEPARTMENTS_URL_;
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}
if (!function_exists('departments_dir')) {

    function departments_dir($subdirectory = '') {
        $link = 'departments/';
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('usertypes_url')) {

    function usertypes_url($subdirectory = '') {
        $link = _USERTYPES_URL_;
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}
if (!function_exists('usertypes_dir')) {

    function usertypes_dir($subdirectory = '') {
        $link = 'user_types/';
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}



/*
 * Uploads URL
 */
if (!function_exists('upload_dir')) {

    function upload_dir($subdirectory = '', $file = '') {
        $link = _UPLOAD_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

if (!function_exists('upload_images_dir')) {

    function upload_images_dir($subdirectory = '', $file = '') {
        $link = _UPLOAD_IMAGES_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}


if (!function_exists('upload_user_dir')) {

    function upload_user_dir($subdirectory = '', $file = '') {
        $link = _UPLOAD_USER_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

if (!function_exists('upload_company_dir')) {

    function upload_company_dir($subdirectory = '', $file = '') {
        $link = _UPLOAD_COMPANY_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

/*
 * Assets URL
 */
if (!function_exists('assets_dir')) {

    function assets_dir($subdirectory = '', $file = '') {
        $link = _ASSETS_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

if (!function_exists('avatars_dir')) {

    function avatars_dir($subdirectory = '', $file = '') {
        $link = _AVATARS_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

if (!function_exists('css_dir')) {

    function css_dir($subdirectory = '', $file = '') {
        $link = _CSS_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

if (!function_exists('font_dir')) {

    function font_dir($subdirectory = '', $file = '') {
        $link = _FONT_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

if (!function_exists('images_dir')) {

    function images_dir($subdirectory = '', $file = '') {
        $link = _IMAGES_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

if (!function_exists('img_dir')) {

    function img_dir($subdirectory = '', $file = '') {
        $link = _IMG_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

if (!function_exists('js_dir')) {

    function js_dir($subdirectory = '', $file = '') {
        $link = _JS_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

/* End of file MY_url_helper.php */
/* Location: ./application/helpers/MY_url_helper.php */
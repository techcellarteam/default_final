<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// --------------------------------------------------------------------

/**
 * Tools Class
 *
 * Extends basic PHP functions
 *
 * */
class Tools {

    public $CI;

    // ------------------------------------------------------------------------

    /*
     * Get POST data
     *
     */
    public function getPost($index = '', $index2 = '') {
        if (!empty($index)) {
            if (!empty($index2) OR is_numeric($index2)) {
                if (isset($_POST[$index][$index2]))
                    return ($_POST[$index][$index2]);
                else
                    return FALSE;
            }
            else {
                if (isset($_POST[$index]))
                    return ($_POST[$index]);
                else
                    return FALSE;
            }
        }


        return $_POST;
    }

    // ------------------------------------------------------------------------

    /*
     * Set POST data
     *
     */
    public function setPost($index = '', $value = '') {
        return $_POST[$index] = $value;
    }

    // ------------------------------------------------------------------------

    /*
     * Set POST data
     *
     */
    public function setPostArray($index = '', $value = array()) {
        return $_POST[$index] = $value;
    }

    // ------------------------------------------------------------------------

    /*
     * Get GET data
     *
     */
    public function getGet($index = '') {
        if ($index != '')
            if (isset($_GET[$index]))
                return ($_GET[$index]);
            else
                return FALSE;

        return $_GET;
    }

    // ------------------------------------------------------------------------

    /*
     * Set POST data
     *
     */
    public function setGet($index = '', $value = '') {
        return $_GET[$index] = $value;
    }
    
    
    public function setTableHeaders(){
        
    }

}

<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

Class Misc {

    function cleanfilename($file) {
        $arr = array('\\', '/', '?');
        return str_replace($arr, " ", $file);
    }

    function generate_password($ctr) {
        $key = "1234567890QWERTYUIOPASDFGHJKLZXCVBNM1234567890QWERTYUIOPASDFGHJKLZXCVBNM!@#$%^&*()_+=-?!@#$%^&*()_+=-?";
        $count = strlen($key);
        $code = "";
        for ($i = 1; $i <= $ctr; $i++) {
            $code.=$key[rand(0, $count - 1)];
        }
        return array(
            'pass' => $code,
            'hash' => md5($code)
        );
    }

    /*
     * Stage status
     */

    function status_stage_label($stage) {
        $data = self::status_stage_prop($stage);
        return (!empty($data['label'])) ? $data['label'] : '';
    }

    function status_stage_icon($stage) {
        $data = self::status_stage_prop($stage);
        return (!empty($data['icon'])) ? $data['icon'] : '';
    }

    function status_stage_color($stage) {
        $data = self::status_stage_prop($stage);
        return (!empty($data['color'])) ? $data['color'] : '';
    }

    function status_stage_prop($stage) {
        $data = array(
            "label" => "",
            "color" => "",
            "icon" => ""
        );
        if ($stage == 0) {
            $data = array(
                "label" => "Not Started",
                "color" => "gray",
                "icon" => "icomoon-icon-minus"
            );
        } else if ($stage == 1) {
            $data = array(
                "label" => "Initiated",
                "color" => "green",
                "icon" => "icomoon-icon-play-3"
            );
        } else if ($stage == 2) {
            $data = array(
                "label" => "Submitted",
                "color" => "blue",
                "icon" => "icomoon-icon-disk"
            );
        } else if ($stage == 3) {
            $data = array(
                "label" => "Locked",
                "color" => "red",
                "icon" => "icomoon-icon-lock"
            );
        } else if ($stage == 4) {
            $data = array(
                "label" => "Completed",
                "color" => "green",
                "icon" => "minia-icon-checkmark-2"
            );
        } else if ($stage == 5) {
            $data = array(
                "label" => "Idle",
                "color" => "gray",
                "icon" => "entypo-icon-sleep"
            );
        } else if ($stage == 6) {
            $data = array(
                "label" => "Started",
                "color" => "green",
                "icon" => "icomoon-icon-play-3"
            );
        }
        return $data;
    }

    function count_value($data) {
        $ctr = 0;
        if (!empty($data))
            if (is_array($data)) {
                foreach ($data as $val) {
                    if (!empty($val)) {
                        $ctr++;
                    }
                }
            }

        return $ctr;
    }

    function batch_decode_id($data) {
        $layer = "";
        if (!empty($data))
            if (is_array($data))
                foreach ($data as $var => $val) {
                    $layer[$var] = self::decode_id($val);
                }
        return $layer;
    }

    function explode_string($delimiter, $data) {
        $layer = "";
        foreach ($data as $var => $val) {
            if (is_string($val) and $val != '') {
                if ($arr = explode($delimiter, $val)) {
                    $layer[$var] = $arr;
                } else {
                    $layer[$var] = array();
                }
            } else {
                $layer[$var] = array();
            }
        }
        return $layer;
    }

    function arr_query($arr, $format, $othercondition = "") {
        $condition = "";
        if (!empty($arr))
            if (is_array($arr)) {
                $i = 0;

                foreach ($arr as $val) {
                    $i++;
                    if ($i != 1)
                        $condition.=" OR ";
                    $condition.=sprintf($format, $val);
                }
                if ($condition) {
                    $condition = "( " . $condition . " )";

                    if ($othercondition) {
                        $condition = "( " . $condition . " AND " . $othercondition . " )";
                    }
                }
            }
        return $condition;
    }

    function generate_code($ctr) {
        $key = "1234567890QWERTYUIOPASDFGHJKLZXCVBNM1234567890QWERTYUIOPASDFGHJKLZXCVBNM";
        $count = strlen($key);
        $code = "";
        for ($i = 1; $i <= $ctr; $i++) {
            $code.=$key[rand(0, $count - 1)];
        }
        return $code;
    }

    function yn($value) {
        if ($value == 'Y') {
            return "Yes";
        } else if ($value == 'N') {
            return "No";
        }
        return "";
    }

    function no_zero($value) {
        if ($value == 0) {
            return "";
        }
        return $value;
    }

    //Get label Data option
    function gldo($data, $key, $code) {
        if (!empty($data[$key][$code])) {
            return $data[$key][$code];
        }
        return $code;
    }

    function decode_json_query($data, $key) {
        if (!empty($data->{$key})) {
            return json_decode($data->{$key});
        }
        return "";
    }

    function decode_json_pointer($data) {
        if (!empty($data)) {
            $arr = json_decode($data);
            $layer = "";
            if (!empty($arr))
                foreach ($arr as $var => $val) {
                    $layer[$val] = $val;
                }
            return $layer;
        }
        return "";
    }

    function default_date() {
        return "1900-01-01";
    }

    function check_str($string, $not_allowed = array(" ")) {
        $str = str_replace($not_allowed, "", $string, $count);

        if ($count) {
            return false;
        }

        return true;
    }

    // function generate_password(){
    // $key="1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM!@#$%&*_+=1234567890!@#$%&*_+=";
    // $count=strlen($key);
    // $pass="";
    // for($i=0;$i<=8;$i++){
    // $pass.=$key[rand(0,$count-1)];
    // }
    // return $pass;
    // }

    function compute_tenure($date) {
        return number_format(((time() - strtotime($date)) / 86400) / 365.242, 2, '.', '');
    }

    function convert_md5($var) {
        return "dflt" . md5($var);
    }

    /*
     * File Icon
     */

    function file_icon($extension) {
        $excel = array('.xls', '.xlt', '.xlm', '.xlsx', '.xlsm', '.xltx', '.xltm', '.xlsb', '.xla', '.xlam', '.xll', '.xlw');
        $doc = array('.docb', '.dotm', '.dotx', '.docm', '.docx', '.dot', '.doc');
        $powerpoint = array('.ppt', '.pot', '.pps', '.pptx', '.pptm', '.potx', '.potm', '.ppam', '.ppsx', '.ppsm', '.sldx', '.sldm');
        $image = array('.tif', '.tiff', '.gif', '.jpeg', '.jpg', '.jif', '.jfif', '.jp2', '.jpx', '.j2k', '.j2c', '.fpx', '.pcd', '.png');
        $pdf = array('.pdf');
        $audio = array('.3gp', '.act', '.aiff', '.aac', '.amr', '.au', '.awb', '.dct', '.dss', '.dvf', '.flac', '.gsm', '.iklax', '.ivs', '.m4a', '.m4p', '.mmf', '.mp3', '.mpc', '.msv', '.ogg', '.oga', '.opus', '.ra', '.rm', '.raw', '.sln', '.tta', '.vox', '.wav', '.wma', '.wv');
        $video = array('.webm', '.mkv', '.flv', '.vob', '.ogv', '.ogg', '.drc', '.mng', '.avi', '.mov', '.qt', '.wmv', '.yuv', '.rm', '.rmvb', '.asf', '.mp4', '.m4p', '.mpg', '.mp2', '.mpeg', '.mpe', '.mpv', '.mpg', '.mpeg', '.m2v', '.svi', '.3gp', '.3g2', '.mxf', '.roq', '.nsv');

        if (in_array($extension, $excel)) {
            return 'icomoon-icon-file-excel';
        } else if (in_array($extension, $doc)) {
            return 'icomoon-icon-file-word';
        } else if (in_array($extension, $powerpoint)) {
            return 'icomoon-icon-file-powerpoint';
        } else if (in_array($extension, $image)) {
            return 'eco-pictures';
        } else if (in_array($extension, $pdf)) {
            return 'icomoon-icon-file-pdf';
        } else if (in_array($extension, $audio)) {
            return 'eco-pictures';
        } else if (in_array($extension, $video)) {
            return 'eco-videos';
        } else {
            return 'icomoon-icon-file-6';
        }
    }

    /* Date Query Ready */

    function cleandate($date, $out = false, $format = "F d Y", $all = false) {
        if (empty($format))
            $format = "F d Y";
        if ($out) {
            if (!$all) {
                if ($date != '0000-00-00' and
                        $date != '1900-01-01' and
                        $date != '' and
                        $date != '0000-00-00 00:00:00' and
                        $date != '1900-01-01 00:00:00' and
                        $date != '1970-01-01' and
                        $date != '1970-01-01  00:00:00') {
                    return date($format, strtotime($date));
                } else {
                    return date($format, strtotime($date));
                }
            } else {
                return date($format, strtotime($date));
            }
        } else {
            if ($date != '') {
                return date('Y-m-d', strtotime($date));
            }
        }
        return "";
    }

    function cleandatetime($date, $out = false, $format = "F d Y h:i A") {
        if ($out) {
            if ($date != '0000-00-00 00:00:00' and $date != '1900-01-01 00:00:00' and $date != '' and $date != '1970-01-01 00:00:00') {
                return date($format, strtotime($date));
            }
        } else {
            if ($date != '') {
                return date('Y-m-d H:i:s', strtotime($date));
            }
        }
        return "";
    }

    /*
     * String
     */

    function oneline_string($str) {
        $delete = array("<p>");
        $str = str_replace($delete, "", $str);
        $delete = array("</p>", "<br/>", "<br />", "<br>", "\n", "\r");
        return str_replace($delete, " ", $str);
    }

    /* new misc */

    function encode_id($id) {
        if ($id)
            return ((($id + $id) * 1234567890) + 100) . "__" . md5(date('Ymdhis') . rand(0, 100));
        return "";
    }

    function decode_id($key) {
        if ($key) {
            $key_arr = explode("__", $key);
            if (!empty($key_arr[0])) {
                return (int) (($key_arr[0] - 100) / 1234567890) / 2;
            }
        }
        return 0;
    }

    function redirect_tab($first = '', $second = '') {
        $link = admin_url($this->uri->rsegment(1) . '/' . $this->uri->rsegment(2) . '/' . $this->uri->rsegment(3));
        if ($first != '')
            $link.="/" . $first;
        if ($second != '')
            $link.="/" . $second;
        return $link;
    }

    function display_name($first, $mid, $last) {
        $name = $first . " ";
        $name.=(trim($mid) != '') ? trim($mid) . " " : "";
        $name.=$last;
        return $name;
    }

    function init_name($first, $mid, $last) {
        $name = $first[0];
        $name.=(trim($mid) != '') ? $mid[0] : "";
        $name.=$last[0];
        return $name;
    }

    function clean_data($data) {
        return trim(htmlspecialchars($data, ENT_QUOTES));
    }

    function accessible($data, $class,  $function) {
        if ($data['user_type_id'] == 1) {
            return true;
        }
        if (!empty($data[$class][$type][$function])) {
            if ($data[$class][$type][$function] == 'true') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function update_name_log($old, $new) {
        if ($old != $new) {
            return $old . " to " . $new;
        } else {
            return $old;
        }
    }

    function query_to_mularr($result, $pointer, $key = '') {
        $layer = '';
        foreach ($result as $q) {
            if ($key) {
                $layer[$q->{$pointer}][] = $q->{$key};
            } else {
                $layer[$q->{$pointer}][] = $q;
            }
        }
        return $layer;
    }

    function query_to_arr($result, $pointer, $key = '') {
        $layer = array();
        foreach ($result as $q) {
            if ($key) {
                $layer[$q->{$pointer}] = $q->{$key};
            } else {
                $layer[$q->{$pointer}] = $q;
            }
        }
        return $layer;
    }

    function query_to_arr2($result, $pointer, $key = '') {
        $layer = '';
        foreach ($result->result() as $q) {
            if ($key) {
                $layer[$q->{$pointer}] = $q->{$key};
            } else {
                $layer[$q->{$pointer}] = $q;
            }
        }
        return $layer;
    }

    function get_enddate($start, $total, $type) {
        $type_name = '';
        if ($type == 1) {
            $type_name = "day";
        } else if ($type == 2) {
            $type_name = "week";
        } else if ($type == 3) {
            $type_name = "month";
        } else {
            $type_name = "year";
        }

        return date('Y-m-d', strtotime($start . " +" . $total . " " . $type_name) - 86400);
    }

    function convert_to_float($number) {
        $remove = array(",", " ");
        return $number = str_replace($remove, "", $number);
    }

    function prezero_number($number, $length) {
        $new_num = '';
        $num_len = strlen($number);
        $num_zero = $length - $num_len;
        if ($num_zero > 0) {
            $new_num = "";
            for ($i = 0; $i < $num_zero; $i++) {
                $new_num.="0";
            }
            $new_num.=$number;
        } else {
            $new_num.=$number;
        }

        return $new_num;
    }

    /*
     * Message Status
     */

    function message_status($type, $message = "", $title = "", $redirect = "") {
        $class = '';
        $str = "";

        if (!$title) {
            $title = self::status_title($type);
        }
        if ($type == 'warning') {
            $class = "alert-warning";
        } else if ($type == 'error') {
            $class = "alert-danger";
        } else if ($type == 'success') {
            $class = "alert-success";
            if (!$message) {
                $message = "Successfully saved";
            }
        } else if ($type == 'info') {
            $class = "alert-info";
        }

        if ($class != '' and $message != '') {
            $div = "<div class='col-lg-12 modal-alert'><div class='alert $class '>
				<button id='redirect' type='button' class='close' data-dismiss='modal'>&times;</button>
				<strong>$title</strong> $message
			</div>
                    </div>";
            if ($redirect) {
                $div .= "<script type='text/javascript'>
                        $(document).ready(function () {
                             $('#redirect').on('click',function() {
                             window.location.assign('$redirect');
                            });
                        });
                    </script>";
            }
            return $div;
        }
        return false;
    }

    function status_title($type) {
        $title = '';
        if ($type == 'warning') {
            $title = "Warning!";
        } else if ($type == 'error') {
            $title = "Sorry!";
        } else if ($type == 'success') {
            $title = "Well done!";
        } else if ($type == 'info') {
            $title = "Heads up!";
        }
        return $title;
    }

    function status_iconnoti($type) {
        $icon = '';
        if ($type == 'warning') {
            $icon = "picon icon16 entypo-icon-warning white";
        } else if ($type == 'error') {
            $icon = "picon icon24 typ-icon-cancel white";
        } else if ($type == 'success') {
            $icon = "picon icon16 iconic-icon-check-alt white";
        } else if ($type == 'info') {
            $icon = "picon icon16 brocco-icon-info white";
        }
        return $icon;
    }

    /* old misc */

    function tschar($string) {
        return htmlspecialchars(trim($string));
    }

    function get_thumbicon($status) {
        $result = "&nbsp;";
        if ($status == "image") {
            $result = "icon-picture btn-pink";
        } else if ($status == "application" or $status == "text") {
            $result = "icon-file btn-warning";
        } else if ($status == "audio") {
            $result = "icon-music btn-purple";
        } else if ($status == "video") {
            $result = "icon-film btn-success";
        }
        return $result;
    }

    function get_thumbicon2($status) {
        $result = "&nbsp;";
        if ($status == "image") {
            $result = "icon-picture";
        } else if ($status == "application" or $status == "text") {
            $result = "icon-file";
        } else if ($status == "audio") {
            $result = "icon-music";
        } else if ($status == "video") {
            $result = "icon-film";
        }
        return $result;
    }

    function get_durationlabel($type, $total) {
        $data = "";
        if ($type == 1) {
            $data = "Day";
        }
        if ($type == 2) {
            $data = "Week";
        }
        if ($type == 3) {
            $data = "Month";
        }
        if ($type == 4) {
            $data = "Year";
        }

        if ($total > 1) {
            $data.="s";
        }
        return $data;
    }

    function get_duration($id) {
        if ($id == 1) {
            return 'Daily';
        }
        if ($id == 2) {
            return 'Weekly';
        }
        if ($id == 3) {
            return 'Monthly';
        }
        if ($id == 4) {
            return 'Yearly';
        }
    }

    function get_scopework($id) {
        if ($id == 1) {
            return 'Rental';
        }
        if ($id == 2) {
            return 'Installation';
        }
        if ($id == 3) {
            return 'Dismantling';
        }
        if ($id == 4) {
            return 'Installation and Dismantling';
        }
    }

    function get_stage($id) {
        if ($id == 1) {
            return 'Request';
        }
        if ($id == 2) {
            return 'Verification Checklist';
        }
        if ($id == 3) {
            return 'Implementation Checklist';
        }
    }

    function get_color($number) {
        $mod = $number % 7;
        if ($mod == 1) {
            $color = 'green';
        } else if ($mod == 2) {
            $color = 'blue';
        } else if ($mod == 3) {
            $color = 'pink';
        } else if ($mod == 4) {
            $color = 'orange';
        } else if ($mod == 5) {
            $color = 'purple';
        } else if ($mod == 6) {
            $color = 'red';
        } else {
            $color = 'grey';
        }
        return $color;
    }

    function get_badge($number) {
        $mod = $number % 7;
        if ($mod == 1) {
            $color = 'success';
        } else if ($mod == 2) {
            $color = 'info';
        } else if ($mod == 3) {
            $color = 'pink';
        } else if ($mod == 4) {
            $color = 'warning';
        } else if ($mod == 5) {
            $color = 'purple';
        } else if ($mod == 6) {
            $color = 'important';
        } else {
            $color = 'grey';
        }
        return $color;
    }

    function convent_date_label($date, $separator) {
        $result = "&nbsp;";
        if ($date != '0000-00-00 00:00:00') {
            $result = date('F d Y', strtotime($date));
            if (date('H:i', strtotime($date)) != '00:00') {
                $result.="$separator" . date('h:i A', strtotime($date));
            }
        }
        return $result;
    }

    function str_chop($str, $max, $cat) {
        $result = '';
        if (strlen($str) > $max) {
            $result = substr($str, 0, $max - strlen($cat)) . $cat;
        } else {
            $result = $str;
        }
        return $result;
    }

    function task_status($status) {
        $result = "&nbsp;";
        if ($status == 1) {
            $result = "<span class='label label-info arrowed-in-right arrowed'>Queued</span>";
        } else if ($status == 2) {
            $result = "<span class='label label-success arrowed'>On Going</span>";
        } else if ($status == 3) {
            $result = "<span class='label label-warning arrowed-in'>On Hold</span>";
        } else if ($status == 4) {
            $result = "<i class='green icon-flag bigger-130'></i> <b>Finished</b>";
        } else if ($status == 5) {
            $result = "<span class='label label-important arrowed-in'>Deleted</span>";
        }
        return $result;
    }

    function task_priority($priority) {
        $result = "";

        for ($i = 0; $i < $priority; $i++) {
            $result.="<i class='icon-star smaller-80 orange'></i>";
        }

        if ($priority == 1) {
            $result.=" Low Priority";
        } else if ($priority == 2) {
            $result.=" Important";
        } else if ($priority == 3) {
            $result.=" Very Important but not Urgent";
        } else if ($priority == 4) {
            $result.=" Urgent";
        } else if ($priority == 5) {
            $result.=" Urgent & Very Important";
        }
        $result.="&nbsp;";
        return $result;
    }

    function convert_taskestimate_time($estimate_type, $estimate_value) {
        $total = 0;
        if ($estimate_type == 1) {
            $total = $estimate_value * 60;
        } else if ($estimate_type == 2) {
            $total = $estimate_value * 60 * 60;
        } else if ($estimate_type == 3) {
            $total = $estimate_value * 60 * 60 * 8;
        } else if ($estimate_type == 4) {
            $total = $estimate_value * 60 * 60 * 8 * 5;
        } else if ($estimate_type == 5) {
            $total = $estimate_value * 60 * 60 * 8 * 20;
        }
        return $total;
    }

    function convert_time_taskestimate($total) {
        $type = '';
        $value = '';
        if ($total == 0) {
            
        } else if ($total < 3600) {
            $type = 1;
            $value = $total / 60;
        } else if ($total <= 28800) {
            $type = 2;
            $value = $total / 60 / 60;
        } else if ($total <= 288000) {
            $type = 3;
            $value = $total / 60 / 60 / 8;
        } else if ($total <= 1152000) {
            $type = 4;
            $value = $total / 60 / 60 / 8 / 5;
        } else if ($total <= 6912000) {
            $type = 5;
            $value = $total / 60 / 60 / 8 / 20;
        }
        $option = '';
        if ($type == 1) {
            $option.="<option value='15'>15 Mins</option>";
            $option.="<option value='30'>30 Mins</option>";
        } else if ($type == 2) {
            $option.="<option value='1'>1 Hour</option>";
            $option.="<option value='2'>2 Hours</option>";
            $option.="<option value='4'>4 Hours</option>";
            $option.="<option value='8'>8 Hours</option>";
        } else if ($type == 3) {
            $option.="<option value='2'>2 Days</option>";
            $option.="<option value='3'>3 Days</option>";
            $option.="<option value='5'>5 Days</option>";
            $option.="<option value='10'>10 Days</option>";
        } else if ($type == 4) {
            $option.="<option value='3'>3 Weeks</option>";
            $option.="<option value='4'>4 Weeks</option>";
            $option.="<option value='6'>6 Weeks</option>";
            $option.="<option value='8'>8 Weeks</option>";
        } else if ($type == 5) {
            $option.="<option value='3'>3 Mons</option>";
            $option.="<option value='4'>4 Mons</option>";
            $option.="<option value='6'>6 Mons</option>";
            $option.="<option value='8'>8 Mons</option>";
            $option.="<option value='10'>10 Mons</option>";
            $option.="<option value='12'>12 Mons</option>";
        }
        $data['type'] = $type;
        $data['value'] = $value;
        $data['option'] = $option;
        return $data;
    }

    function get_date_plural($day) {
        if ($day != '' and ( $day > 1 or $day < -1)) {
            return ' days';
        } else {
            return ' day';
        }
    }

    function get_date_array($date) {
        $start = date('Y-m', strtotime($date)) . '-01';
        $end = date('Y-m', strtotime($date)) . '-' . date('t', strtotime($date));
        $layer = '';
        $i = 0;
        for ($x = strtotime($start); $x <= strtotime($end); $x = $x + 86400) {
            $layer[$i++] = date('Y-m-d', $x);
        }
        return $layer;
    }

    function get_date_array2($start, $end) {
        $start = date('Y-m-d', strtotime($start));
        $end = date('Y-m-d', strtotime($end));
        $layer = '';
        $i = 0;
        for ($x = strtotime($start); $x <= strtotime($end); $x = $x + 86400) {
            $layer[$i++] = date('Y-m-d', $x);
        }
        return $layer;
    }

    function minus_day($start, $end) {
        if ($start != '' and $end != '') {
            $start = date('Y-m-d', strtotime($start));
            $end = date('Y-m-d', strtotime($end));
            $secs = (strtotime($end) - strtotime($start));
            $days = $secs / 86400;
            if ($days >= 0) {
                $days++;
            }
        } else {
            $days = 0;
        }
        return $days;
    }

    function get_curnextprev_month($date) {
        $start = date('Y-m', strtotime($date)) . '-01';
        $end = date('Y-m', strtotime($date)) . '-' . date('t', strtotime($date));

        $layer['prev'] = date('Y-m', strtotime($start) - 86400);
        $layer['next'] = date('Y-m', strtotime($end) + 86400);
        $layer['cur'] = date('Y-m', strtotime($date));
        return $layer;
    }

    function convert_time($sec) {
        $time = '';
        $month = 0;
        $week = 0;
        $day = 0;
        $hour = 0;
        $minute = 0;
        $second = 0;
        $month = floor(((($sec / 60) / 60) / 8) / 20);
        $rmonth = fmod(((($sec / 60) / 60) / 8), 20);
        if ($rmonth > 0) {
            $sec = $sec - ($month * 60 * 60 * 8 * 20);
            $week = floor(((($sec / 60) / 60) / 8) / 5);
            $rweek = fmod(((($sec / 60) / 60) / 8), 5);
            if ($rweek > 0) {
                $sec = $sec - ($week * 60 * 60 * 8 * 5);
                $day = floor((($sec / 60) / 60) / 8);
                $rday = fmod((($sec / 60) / 60), 8);
                if ($rday > 0) {
                    $sec = $sec - ($day * 60 * 60 * 8);
                    $hour = floor(($sec / 60) / 60);
                    $rhour = fmod(($sec / 60), 60);
                    if ($rhour > 0) {
                        $sec = $sec - ($hour * 60 * 60);
                        $minute = floor($sec / 60);
                        $rminute = fmod($sec, 60);
                        if ($rminute > 0) {
                            $second = $sec - ($minute * 60);
                        }
                    }
                }
            }
        }
        if ($month > 0) {
            if ($month == 1) {
                $time.=$month . ' month ';
            } else {
                $time.=$month . ' months ';
            }
        }
        if ($week > 0) {
            if ($week == 1) {
                $time.=$week . ' week ';
            } else {
                $time.=$week . ' weeks ';
            }
        }
        if ($day > 0) {
            if ($day == 1) {
                $time.=$day . ' day ';
            } else {
                $time.=$day . ' days ';
            }
        }
        if ($hour > 0) {
            if ($hour == 1) {
                $time.=$hour . ' hour ';
            } else {
                $time.=$hour . ' hours ';
            }
        }
        if ($minute > 0) {
            if ($minute == 1) {
                $time.=$minute . ' min ';
            } else {
                $time.=$minute . ' mins ';
            }
        }
        if ($second > 0) {
            if ($second == 1) {
                $time.=$second . ' sec ';
            } else {
                $time.=$second . ' secs ';
            }
        }

        return $time;
    }

    function compute_age($date) {
        $sec = strtotime($date);
        $age = (date('md', $sec) > date('md')) ? ((date('Y') - date('Y', $sec)) - 1) : (date('Y') - date('Y', $sec));
        return $age;
    }

    function convert_twodate_label_mini($start, $end, $separator) {
        $result = '';
        if ($start != '0000-00-00 00:00:00' and $end != '0000-00-00 00:00:00' and $start != '0000-00-00' and $end != '0000-00-00') {
            if ($start == $end) {
                $result = date('M j', strtotime($start));
            } else if (date('MY', strtotime($start)) == date('MY', strtotime($end))) {
                $result = date('M j', strtotime($start)) . " $separator " . date(' j', strtotime($end));
            } else if (date('Y', strtotime($start)) == date('Y', strtotime($end))) {
                $result = date('M j', strtotime($start)) . " $separator " . date('M j', strtotime($end));
            } else {
                $result = date('M j', strtotime($start)) . " $separator " . date('M j', strtotime($end));
            }
        } else if ($start != '0000-00-00 00:00:00' and $start != '0000-00-00') {
            $result = date('M j', strtotime($start));
        }

        return $result;
    }

    function convert_twodate_label($start, $end, $separator) {
        $result = '';

        if ($start != '0000-00-00 00:00:00' and $end != '0000-00-00 00:00:00' and $start != '0000-00-00' and $end != '0000-00-00') {
            $start = date('Y-m-d', strtotime($start));
            $end = date('Y-m-d', strtotime($end));
            if ($start == $end) {
                $result = date('F j Y', strtotime($start));
            } else if (date('MY', strtotime($start)) == date('MY', strtotime($end))) {
                $result = date('M j', strtotime($start)) . " $separator " . date(' j Y', strtotime($end));
            } else if (date('Y', strtotime($start)) == date('Y', strtotime($end))) {
                $result = date('M j', strtotime($start)) . " $separator " . date('M j Y', strtotime($end));
            } else {
                $result = date('M j Y', strtotime($start)) . " $separator " . date('M j Y', strtotime($end));
            }
        } else if ($start != '0000-00-00 00:00:00' and $start != '0000-00-00') {
            $result = date('F j Y', strtotime($start));
        }

        return $result;
    }

    function convert_twodatetime_label($start, $end, $separator) {
        $result = '';
        if ($start != '0000-00-00 00:00:00' and $end != '0000-00-00 00:00:00') {
            $startdate = date('Y-m-d', strtotime($start));
            $enddate = date('Y-m-d', strtotime($end));

            if ($startdate == $enddate) {
                $result = date('F j Y, g:i A', strtotime($start)) . " $separator " . date('g:i A', strtotime($end));
            } else {
                $result = date('M j Y, g:i A', strtotime($start)) . " $separator " . date('M j Y, g:i A', strtotime($end));
            }
        } else if ($start != '0000-00-00 00:00:00') {
            $result = date('F j Y, g:i A', strtotime($start));
        }

        return $result;
    }

    function convert_time_label($datetime, $format) {
        $time_datetime = strtotime(date('Y-m-d H:i', strtotime($datetime)));
        $time_cdatetime = strtotime(date('Y-m-d H:i'));
        if ($time_cdatetime - $time_datetime <= 60) {
            $result = "few seconds ago";
        } else if ($time_cdatetime - $time_datetime > 60 and $time_cdatetime - $time_datetime < 3600) {
            $result = (($time_cdatetime - $time_datetime) / 60) . " minutes ago";
        } else if ($time_cdatetime - $time_datetime >= 3600 and $time_cdatetime - $time_datetime < 7200) {
            $result = " about an hour ago";
        } else if ($time_cdatetime - $time_datetime >= 7200 and $time_cdatetime - $time_datetime < 43200) {
            $result = ceil((($time_cdatetime - $time_datetime) / 60) / 60) . " hours ago";
        } else {
            $result = date($format, $time_datetime);
        }
        return $result;
    }

    function convert_time_label2($datetime, $format) {
        $time_datetime = strtotime(date('Y-m-d H:i', strtotime($datetime)));
        $time_cdatetime = strtotime(date('Y-m-d H:i'));
        $result = '';
        if ($time_cdatetime - $time_datetime <= 60) {
            $result = "few seconds ago : ";
        } else if ($time_cdatetime - $time_datetime > 60 and $time_cdatetime - $time_datetime < 3600) {
            $result = (($time_cdatetime - $time_datetime) / 60) . " minutes ago : ";
        } else if ($time_cdatetime - $time_datetime >= 3600 and $time_cdatetime - $time_datetime < 7200) {
            $result = " about an hour ago : ";
        } else if ($time_cdatetime - $time_datetime >= 7200 and $time_cdatetime - $time_datetime < 43200) {
            $result = ceil((($time_cdatetime - $time_datetime) / 60) / 60) . " hours ago : ";
        }
        $result.=date($format, $time_datetime);
        return $result;
    }

    function convert_time_hhmm($sec) {
        if ($sec > 0) {
            $hour = 0;
            $minute = 0;
            $hour = floor(($sec / 60) / 60);
            $rhour = fmod(($sec / 60), 60);
            if ($rhour > 0) {
                $sec = $sec - ($hour * 60 * 60);
                $minute = floor($sec / 60);
                $rminute = fmod($sec, 60);
                if ($rminute > 0) {
                    $second = $sec - ($minute * 60);
                }
            }
            $data['hour'] = $hour;
            $data['min'] = $minute;
        } else {
            $data['hour'] = '';
            $data['min'] = '';
        }
        return $data;
    }

    function convert_time_hhmm_str($sec) {
        $hour = 0;
        $minute = 0;
        $hour = floor(($sec / 60) / 60);
        $rhour = fmod(($sec / 60), 60);
        if ($rhour > 0) {
            $sec = $sec - ($hour * 60 * 60);
            $minute = floor($sec / 60);
            $rminute = fmod($sec, 60);
            if ($rminute > 0) {
                $second = $sec - ($minute * 60);
            }
        }

        $result = '';

        if ($hour > 0) {
            if ($hour == 1) {
                $result.=$hour . ' hour ';
            } else {
                $result.=$hour . ' hours ';
            }
        }

        if ($minute > 0) {
            if ($minute == 1) {
                $result.=$minute . ' min ';
            } else {
                $result.=$minute . ' mins ';
            }
        }

        return $result;
    }

    function minus_time($start, $end) {
        if ($start != '' and $end != '') {
            $date = date('Y-m-d');
            if (strtotime($date . ' ' . $start) <= strtotime($date . ' ' . $end)) {
                $shift_start = $date . ' ' . $start;
                $shift_end = $date . ' ' . $end;
            } else {
                $shift_start = $date . ' ' . $start;
                $shift_end = date('Y-m-d', strtotime($date) + 86400) . ' ' . $end;
            }

            $sec = strtotime($shift_end) - strtotime($shift_start);
            $hour = 0;
            $minute = 0;
            $hour = floor(($sec / 60) / 60);
            $rhour = fmod(($sec / 60), 60);
            if ($rhour > 0) {
                $sec = $sec - ($hour * 60 * 60);
                $minute = floor($sec / 60);
                $rminute = fmod($sec, 60);
                if ($rminute > 0) {
                    $second = $sec - ($minute * 60);
                }
            }
            $data['hour'] = $hour;
            $data['min'] = $minute;
            $data['total_sec'] = strtotime($shift_end) - strtotime($shift_start);
        } else {
            $data['hour'] = '';
            $data['min'] = '';
            $data['total_sec'] = 0;
        }
        return $data;
    }

    function minus_datetime($start, $end) {
        if (strtotime($end) > strtotime($start)) {
            $sec = strtotime($end) - strtotime($start);
            $hour = 0;
            $minute = 0;
            $hour = floor(($sec / 60) / 60);
            $rhour = fmod(($sec / 60), 60);
            if ($rhour > 0) {
                $sec = $sec - ($hour * 60 * 60);
                $minute = floor($sec / 60);
                $rminute = fmod($sec, 60);
                if ($rminute > 0) {
                    $second = $sec - ($minute * 60);
                }
            }
            $data['hour'] = $hour;
            $data['min'] = $minute;
            $data['total_sec'] = strtotime($end) - strtotime($start);
        } else {
            $data['hour'] = '';
            $data['min'] = '';
            $data['total_sec'] = 0;
        }
        return $data;
    }

    function minus_multitime($time) {
        $thour = 0;
        $tmin = 0;
        foreach ($time as $var => $val) {
            $start = $val['start'];
            $end = $val['end'];
            if ($start != '' and $end != '') {
                $date = date('Y-m-d');
                if (strtotime($date . ' ' . $start) <= strtotime($date . ' ' . $end)) {
                    $shift_start = $date . ' ' . $start;
                    $shift_end = $date . ' ' . $end;
                } else {
                    $shift_start = $date . ' ' . $start;
                    $shift_end = date('Y-m-d', strtotime($date) + 86400) . ' ' . $end;
                }

                $sec = strtotime($shift_end) - strtotime($shift_start);
                $hour = 0;
                $minute = 0;
                $hour = floor(($sec / 60) / 60);
                $rhour = fmod(($sec / 60), 60);
                if ($rhour > 0) {
                    $sec = $sec - ($hour * 60 * 60);
                    $minute = floor($sec / 60);
                    $rminute = fmod($sec, 60);
                    if ($rminute > 0) {
                        $second = $sec - ($minute * 60);
                    }
                }
                $thour+=$hour;
                $tmin+=$minute;
            }
        }
        if ($thour == 0 and $tmin == 0) {
            $data['hour'] = '';
            $data['min'] = '';
        } else {
            $data['hour'] = $thour;
            $data['min'] = $tmin;
        }
        return $data;
    }

    function total_worktime($data) {
        $start = $data['start'];
        $end = $data['end'];
        $break_hour = 0;
        $break_min = 0;
        $tbreak = $_POST['totalbreak'];
        if ($tbreak != '') {
            $break = explode(":", $tbreak);
            if (!empty($break[0])) {
                $break_hour = $break[0] * 60 * 60;
            }

            if (!empty($break[1])) {
                $break_min = $break[1] * 60;
            }
        }

        if ($start and $end != '') {
            $date = date('Y-m-d');
            if (strtotime($date . ' ' . $start) <= strtotime($date . ' ' . $end)) {
                $shift_start = $date . ' ' . $start;
                $shift_end = $date . ' ' . $end;
            } else {
                $shift_start = $date . ' ' . $start;
                $shift_end = date('Y-m-d', strtotime($date) + 86400) . ' ' . $end;
            }

            $sec = (strtotime($shift_end) - strtotime($shift_start)) - ($break_hour + $break_min);

            $hour = 0;
            $minute = 0;
            $hour = floor(($sec / 60) / 60);
            $rhour = fmod(($sec / 60), 60);
            if ($rhour > 0) {
                $sec = $sec - ($hour * 60 * 60);
                $minute = floor($sec / 60);
                $rminute = fmod($sec, 60);
                if ($rminute > 0) {
                    $second = $sec - ($minute * 60);
                }
            }
            $data['hour'] = $hour;
            $data['min'] = $minute;
        } else {
            $arr['hour'] = '';
            $arr['min'] = '';
        }
        return $data;
    }

    function asort(&$array, $key) {
        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        return $array = $ret;
    }

    function arsort(&$array, $key) {
        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        arsort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        return $array = $ret;
    }

    /*
     * Convert interget to string
     *
     */

    function convert_number_to_words($number) {

        $hyphen = '-';
        $conjunction = ' and ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' point ';
        $dictionary = array(
            0 => 'zero',
            1 => 'one',
            2 => 'two',
            3 => 'three',
            4 => 'four',
            5 => 'five',
            6 => 'six',
            7 => 'seven',
            8 => 'eight',
            9 => 'nine',
            10 => 'ten',
            11 => 'eleven',
            12 => 'twelve',
            13 => 'thirteen',
            14 => 'fourteen',
            15 => 'fifteen',
            16 => 'sixteen',
            17 => 'seventeen',
            18 => 'eighteen',
            19 => 'nineteen',
            20 => 'twenty',
            30 => 'thirty',
            40 => 'fourty',
            50 => 'fifty',
            60 => 'sixty',
            70 => 'seventy',
            80 => 'eighty',
            90 => 'ninety',
            100 => 'hundred',
            1000 => 'thousand',
            1000000 => 'million',
            1000000000 => 'billion',
            1000000000000 => 'trillion',
            1000000000000000 => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) OR (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int) ($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }

    // ------------------------------------------------------------------------

    /*
     * Generate String from random characters
     *
     */
    function my_htmlspecialchars_decode($string, $style = ENT_COMPAT) {
        $translation = array_flip(get_html_translation_table(HTML_SPECIALCHARS, $style));
        if ($style === ENT_QUOTES) {
            $translation['&#039;'] = '\'';
        }
        return strtr($string, $translation);
    }

    // ------------------------------------------------------------------------

    /*
     * Generate String from random characters
     *
     */
    function safe_html($string) {
        return htmlspecialchars($string, ENT_QUOTES);
    }

    // ------------------------------------------------------------------------

    /*
     * Generate String from random characters
     *
     */
    function decode_safe_html($string, $style = ENT_COMPAT) {
        $translation = array_flip(get_html_translation_table(HTML_SPECIALCHARS, $style));
        if ($style === ENT_QUOTES) {
            $translation['&#039;'] = '\'';
        }
        return strtr($string, $translation);
    }

    // ------------------------------------------------------------------------

    /*
     * Generate a safe Array for SQL QUERY from $_POST
     *
     */
    function make_array_safe_for_SQL($array) {
        foreach ($array as $key => $value)
            $array[$key] = $this->safe_html($value);

        return $array;
    }

    // ------------------------------------------------------------------------

    /*
     * Clean URL_ENCODE
     *
     */
    function clean_url($URL, $type = TRUE) {
        $entities = array('%20', '%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
        $replacements = array(' ', '!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");

        if ($type)
            return str_replace($entities, $replacements, $URL);
        else
            return str_replace($replacements, $entities, $URL);
    }

    // ------------------------------------------------------------------------

    /*
     * Display a calendar view 
     *
     */
    function generate_calendar($date_now) {
        //variable to hold the calendar
        $calendar = "";
        //This gets today's date 
        $now = strtotime($date_now);
        $date = $now;

        //This puts the day, month, and year in seperate variables
        $day = date('d', $date);
        $month = date('m', $date);
        $year = date('Y', $date);

        //Here we generate the first day of the month 
        $first_day = mktime(0, 0, 0, $month, 1, $year);

        //This gets us the month name 
        $title = date('F', $first_day);

        //Here we find out what day of the week the first day of the month falls on 
        $day_of_week = date('D', $first_day);

        //Once we know what day of the week it falls on, we know how many blank days occure before it. If the first day of the week is a Sunday then it would be zero

        switch ($day_of_week) {

            case "Sun": $blank = 0;
                break;

            case "Mon": $blank = 1;
                break;

            case "Tue": $blank = 2;
                break;

            case "Wed": $blank = 3;
                break;

            case "Thu": $blank = 4;
                break;

            case "Fri": $blank = 5;
                break;

            case "Sat": $blank = 6;
                break;
        }

        //We then determine how many days are in the current month

        $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year)); //cal_days_in_month(0, $month, $year) ; 
        //Here we start building the table heads 

        $calendar = "<table border=1 width=294>";

        $calendar = $calendar . "<tr><th colspan=7> $title $year </th></tr>";

        $calendar = $calendar . "<tr><td width=42>S</td><td width=42>M</td><td 
        width=42>T</td><td width=42>W</td><td width=42>T</td><td 
        width=42>F</td><td width=42>S</td></tr>";



        //This counts the days in the week, up to 7

        $day_count = 1;



        $calendar = $calendar . "<tr>";

        //first we take care of those blank days

        while ($blank > 0) {

            $calendar = $calendar . "<td></td>";

            $blank = $blank - 1;

            $day_count++;
        }

        //sets the first day of the month to 1 

        $day_num = 1;



        //count up the days, untill we've done all of them in the month

        while ($day_num <= $days_in_month) {

            $calendar = $calendar . "<td> $day_num </td>";

            $day_num++;

            $day_count++;



            //Make sure we start a new row every week

            if ($day_count > 7) {

                $calendar = $calendar . "</tr><tr>";

                $day_count = 1;
            }
        }

        //Finaly we finish out the table with some blank details if needed

        while ($day_count > 1 && $day_count <= 7) {

            $calendar = $calendar . "<td> </td>";

            $day_count++;
        }


        $calendar = $calendar . "</tr></table>";

        return $calendar;
    }

    // ------------------------------------------------------------------------

    /*
     * Return all value from DOCTYPE
     *
     */
    public function get_file_extension() {
        $ext = "csv|xls|ppt|zip|mp3|mp2|mid|mpga|wav|gif|jpeg|jpg|jpe|png|tiff|tif|txt|text|mpeg|mpg|mpe|qt|mov|avi|movie|doc|docx|word|pdf|xlsx";
        return $ext;
    }

    // ------------------------------------------------------------------------

    /*
     * Generate readable string to special char vise versa
     *
     */
    public function string_to_char($char = "", $stype = TRUE) {

        // special character
        $listChar = array(
            '#', 'Ë', 'Ø', '[', 'ÿ', '~', '¦', '®', '¶', 'Ç',
            '$', '-', '´', ']', ',', '¡', 'ç', '°', '¼', 'Ð',
            '%', '.', '>', 'Ñ', 'Û', '¢', '©', '±', '½', '×',
            '&', 'Æ', '?', '{', 'Ù', '£', 'ª', '²', 'Þ', 'Õ',
            '(', ':', '÷', '|', '^', '¤', '«', '³', 'Ú', 'æ',
            ')', ';', '@', '}', '¾', '¥', '¬', 'µ', '¿', 'ß',
            '*', '<',
        );
        //!\^ËÝçðø÷´
        // readable character
        $listMainChar = array(
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
            'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
            'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
            'y', 'z',
        );
        //echo $char.'<br/>';
        if ($stype == TRUE)
            return str_replace($listMainChar, $listChar, $char);
        elseif ($stype == FALSE)
            return str_replace($listChar, $listMainChar, $char);
    }

    // ------------------------------------------------------------------------

    /*
     * Hash array [Encryption]
     *
     */
    public function hashmenot($arr = '') {
        if (is_array($arr))
            if (!array_key_exists('firsttag', $arr)) {
                $arr = array_merge(array('firsttag' => 'tehcellar'), $arr);
                $arr['lasttag'] = 'solutions';
            }

        $str = base64_encode(serialize(wddx_serialize_value($arr)));
        $strenc = base64_encode(serialize(urlencode($str)));

        return $strenc;
    }

    // ------------------------------------------------------------------------

    /*
     * Hash array [Dencryption]
     *
     */
    public function unhashmenot($str = "") {
        $unserialized_str = unserialize(base64_decode($str));

        $decode_str = urldecode($unserialized_str);

        $final_str = wddx_deserialize(unserialize(base64_decode($decode_str)));

        return $final_str;
    }

    /*
     * SEND TO PARTY PARTY
     * 
     * 
     */

    function party_share($url = '', $data = []) {
        $fields = $data;

        $fields_string = http_build_query($fields);
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    function payment_label($payment) {
        if ($payment == 1) {
            return "Ipay";
        } elseif ($payment == 2) {
            return "Dragonpay";
        } elseif ($payment == 3) {
            return "Bancnet";
        } elseif ($payment == 4) {
            return "Cash";
        } elseif ($payment == 5) {
            return "Credit Card";
        } else {
            return "OTHER";
        }
    }

    function date_paid($date) {
        if ($date == "0000-00-00 00:00:00") {
            return "Pending";
        } else {
            return date('F j, Y, g:i a', strtotime($date));
        }
    }

    function get_pagination_data($data) {
        /* Button and Pages Initialization */
        if ($data['list_count'] > 0) {
            $start_rowcount = (($data['page'] - 1) * $data['display']) + 1;
            $end_rowcount = (($data['page'] - 1) * $data['display']) + count($data['list']->result());
            $data['rowcount'] = array('start' => $start_rowcount, 'end' => $end_rowcount);

            $batch_button = ceil($data['page'] / $data['num_button']);
            $start_button = (($batch_button - 1) * $data['num_button']) + 1;
            $end_button = (($batch_button - 1) * $data['num_button']) + $data['num_button'];
            if ($end_button > $data['max_page']) {
                $end_button = $end_button - ($end_button - $data['max_page']);
            }
            $next_button = $data['page'] + 1;
            if ($next_button > $data['max_page']) {
                $next_button = 0;
            }
            $prev_button = $data['page'] - 1;
            if ($prev_button <= 0) {
                $prev_button = 0;
            }
            $data['button'] = array('start' => $start_button, 'end' => $end_button, 'next' => $next_button, 'prev' => $prev_button);
        }
        return $data;
    }

}

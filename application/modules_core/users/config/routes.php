<?php

/* ------------------------------------------------------------------------------- */
/* ------------------------------- Routes for Users ------------------------------ */
/* ------------------------------------------------------------------------------- */

$route['users/list'] = "users/list_users";
$route['users/view_user/(:any)'] = "users/view_user/$1";
$route['users/add_user'] = "users/add_user";
$route['users/edit_user/(:any)'] = "users/edit_user/$1";
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends System_Core {

    function __construct() {
        parent:: __construct();
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);

        $this->load->model('departments/departments_model');
        $this->load->model('users/users_model');
        $this->load->model('user_types/user_types_model');
    }

    /* -------------------------------------------------------------------- */
    /* ------------------------------- Pages ------------------------------ */
    /* -------------------------------------------------------------------- */

    function add_user() {
        $data = array(
            'template' => parent::main_template(),
            'css_files' => $this->config->item('css_for_validation'),
            'js_files' => $this->config->item('js_for_validation'),
            'user_types' => $this->user_types_model->getSearch("", "", array("user_type" => "ASC"), true),
            'departments' => $this->departments_model->getSearch("", "", array("department_name" => "ASC"), true),
        );
        $this->load->view(users_dir('users/add'), $data);
    }

    function edit_user() {
        $id_user = $this->Misc->decode_id($this->uri->rsegment(3));

        /* Check User if Exist */
        $row = $this->users_model->getFields($id_user);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'css_files' => $this->config->item('css_for_validation'),
                'js_files' => $this->config->item('js_for_validation'),
                'row' => $row,
                'user_types' => $this->user_types_model->getSearch("", "", array("user_type" => "ASC"), true),
                'departments' => $this->departments_model->getSearch("", "", array("department_name" => "ASC"), true),
            );
            $this->load->view(users_dir('users/edit'), $data);
        } else {
            redirect(system_dir('profile/view_pagenotfound_page'));
        }
    }

    function view_user() {
        $id_user = $this->Misc->decode_id($this->uri->rsegment(3));

        /* Check User if Exist */
        $row = $this->users_model->getFields($id_user);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
                'user_types' => $this->user_types_model->getSearch("", "", array("user_type" => "ASC"), true),
                'departments' => $this->departments_model->getSearch("", "", array("department_name" => "ASC"), true),
            );
            $this->load->view(users_dir('users/view'), $data);
        } else {
            redirect(system_dir('profile/view_pagenotfound_page'));
        }
    }

    function list_users() {
        $data = array(
            'template' => parent::main_template(),
            'css_files' => $this->config->item('css_for_tables'),
            'js_files' => $this->config->item('js_for_tables'),
        );
        $this->load->view(users_dir('users/list'), $data);
    }

    /* --------------------------------------------------------------------- */
    /* ------------------------------- Method ------------------------------ */
    /* --------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_users') { /* Method for User */
            self::_method_list_users();
        } else if ($this->uri->rsegment(3) == 'add_user') {
            self::_method_add_user();
        } else if ($this->uri->rsegment(3) == 'edit_user') {
            self::_method_edit_user();
        } else if ($this->uri->rsegment(3) == 'delete_user') {
            self::_method_delete_user();
        }
    }

    /*
     * 	List User
     */

    function _method_list_users() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(system_url($this->classname));
        }

        $data = array(
            'search' => $this->input->post('search'),
            'sort' => $this->input->post('sort'),
        );

        /* Default Order */
        $sort_data = array(); //default sorting
        if (!empty($this->input->post('sort'))) {
            $sort = $this->input->post('sort');
            $sort_by = $sort['sort_by'];
            $sort_type = $sort['sort_type'];
            switch ($sort_by) {
                case 'user_name':
                    $sort_by = 'user_mname';
                    break;
                default:
                    # code...
                    break;
            }
            $sort_data = array_merge($sort_data, array($sort_by => $sort_type));
        }

        //set condition for the list
        $condition = array();
        $string_condition = array();


        if (!empty($this->input->post('search'))) {
            foreach ($this->input->post('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_user') {
                        $condition = array_merge($condition, array($var => $val));
                    } else if ($var == 'user_name') {
                        $condition = array_merge($condition, array("`user_fname` LIKE '%$val%' OR `user_mname` LIKE '%$val%' OR user_lname LIKE '%$val%'" => NULL));
                    } else if ($var == 'user_type') {
                        $condition = array_merge($condition, array("`user_type` LIKE '%$val%'" => NULL));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%$val%"));
                    }
                }
            }
        }

        $data['list_count'] = $this->users_model->getList($condition, $string_condition, $sort_data)->num_rows();

        $data['list'] = $this->users_model->getListLimit($condition, $string_condition, $sort_data, $this->page, $this->display);

        $datas = array('page' => $this->page,
            'display' => $this->display,
            'num_button' => $this->num_button,
            'list_count' => $data['list_count'],
            'list' => $data['list'],
            'max_page' => ((ceil($data['list_count'] / $this->display)) <= 0) ? 1 : (ceil($data['list_count'] / $this->display)),
        );

        $details = array_merge($data, $this->Misc->get_pagination_data($datas)); //get pagination data
        $this->load->view(users_dir('list/user_list'), $details);
    }

    function _method_edit_user() {
        if (IS_AJAX) {

            $id_user = $this->Misc->decode_id($this->input->post('id'));
            /* Check User if Exist */
            $row = $this->users_model->getFields($id_user);
            if ($row) {
                $this->form_validation->set_rules('fname', 'First Name', 'htmlspecialchars|trim|required');
                $this->form_validation->set_rules('mname', 'Middle Name', 'htmlspecialchars|trim|required');
                $this->form_validation->set_rules('lname', 'Last Name', 'htmlspecialchars|trim|required');
                $this->form_validation->set_rules('street', 'Street', 'htmlspecialchars|trim');
                $this->form_validation->set_rules('city', 'City', 'htmlspecialchars|trim');
                $this->form_validation->set_rules('province', 'Province', 'htmlspecialchars|trim');
                $this->form_validation->set_rules('country', 'Country', 'htmlspecialchars|trim');
                $this->form_validation->set_rules('contact', 'Contact No', 'htmlspecialchars|trim');
                $this->form_validation->set_rules('usertype[]', 'User Type', 'htmlspecialchars|trim|required');
                $this->form_validation->set_rules('email', 'Email', 'htmlspecialchars|trim|valid_email');
                $this->form_validation->set_rules('password', 'Password', 'htmlspecialchars|trim');
                $this->form_validation->set_rules('department', 'Department', 'htmlspecialchars|trim');

                if ($this->form_validation->run() == FALSE) {
                    $arr = array(
                        'message_alert' => validation_errors(),
                        'redirect' => $this->input->post('redirect'),
                        'status' => "error",
                        'id' => 0
                    );
                } else {
                    /* Check User Exist */
                    if (!empty($this->input->post('email'))) {
                        $user = $this->users_model->getSearch(array("user_email like" => $this->input->post('email')), "", "", "", "", true);
                        if ($user->id_user == $id_user) {
                            $count = 0;
                        } else {
                            $count = 1;
                        }
                    } else {
                        $count = 0;
                    }

                    if ($count == 0) {
                        $datas = array(
                            'user_type_id' => $this->input->post('usertype'),
                            'user_fname' => $this->input->post('fname'),
                            'user_lname' => $this->input->post('lname'),
                            'user_mname' => $this->input->post('mname'),
                            'user_street' => $this->input->post('street'),
                            'user_city' => $this->input->post('city'),
                            'user_province' => $this->input->post('province'),
                            'user_country' => $this->input->post('country'),
                            'user_contact' => $this->input->post('contact'),
                            'user_email' => $this->input->post('email'),
                            'department_id' => $this->input->post('department'),
                            'updated_by' => $this->my_session->get('admin', 'user_id'),
                            'updated_date' => date('Y-m-d H:i:s')
                        );

                        if (!empty($this->input->post('password')))
                            $datas['user_password'] = sha1($this->input->post('password'));

                        /* Update User Info */
                        $this->users_model->update_table($datas, array("id_user" => $id_user));

                        parent::save_log($id_user, "updated user " . $this->Misc->update_name_log($this->Misc->display_name($row->user_fname, $row->user_mname, $row->user_lname), $this->Misc->display_name($this->input->post('fname'), $this->input->post('mname'), $this->input->post('lname'))));
                        $arr = array(
                            'message_alert' => "Successfully updated user.",
                            'redirect' => $this->input->post('redirect'),
                            'status' => "success",
                            'id' => $id_user
                        );
                    }else {
                        $arr = array(
                            'message_alert' => "Email already exists.",
                            'redirect' => $this->input->post('redirect'),
                            'status' => "error",
                            'id' => 0
                        );
                    }
                }
                $this->load->view(system_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Delete User Method
     */

    function _method_delete_user() {
        if (IS_AJAX) {
            $id_user = $this->Misc->decode_id($this->input->post('item'));
            /* Check User if Exist */
            $row = $this->users_model->getFields($id_user);
            if ($row) {
                $datas = array(
                    'enabled' => 0,
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete User */
                $this->users_model->update_table($datas, array("id_user" => $id_user));
                parent::save_log($id_user, "deleted user " . $this->Misc->display_name($row->user_fname, $row->user_mname, $row->user_lname));
                $arr = array(
                    'message_alert' => "User $row->user_code Deleted ",
                    'redirect' => $this->input->post('redirect'),
                    'status' => "success",
                    'id' => $id_user
                );
                $this->load->view(system_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

}

?>

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Template extends System_Core {

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function confirmation() {
        $id = @$this->input->post('id')? : "NULL";
        $item = @$this->input->post('item')? : "NULL";
        $data = array(
            'item' => $item,
            'id' => $id,
            'message' => $this->input->post('message'),
            'action' => $this->input->post('action'),
            'redirect' => $this->input->post('redirect')
        );

        $this->load->view(system_dir('template/confirmation'), $data);
    }

}

<?php

/* ------------------------------------------------------------------------------------ */
/* ------------------------------- Routes for User Types ------------------------------ */
/* ------------------------------------------------------------------------------------ */
$route['user_types/list'] = "user_types/list_user_type";
$route['user_types/view/(:any)'] = "user_types/view_user_type/$1";
$route['user_types/add'] = "user_types/add_user_type";
$route['user_types/edit/(:any)'] = "user_types/edit_user_type/$1";


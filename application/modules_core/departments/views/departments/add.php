<?php $this->load->view(system_dir('template/header')); ?>
<!--Body content-->
<div class="right_col" role="main">
    <div id="clearflash">
        <div class="page-title">
            <div class="title_left">&nbsp;</div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 pull-right" style="margin: 0;">
                    <div class="x_content">
                        <a href="<?php echo departments_url('list'); ?>"  class="btn btn-round btn-primary" style="float: right;"><span class='fa fa-undo'></span> Back</a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div><!-- end of page-title -->
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add New Department</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal form-label-left" novalidate>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="formdata_deptname">Department Name<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="formdata_deptname" class="form-control formdata col-md-7 col-xs-12" type="text">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="formdata_deptcode">Department Code <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="formdata_deptcode" class="form-control formdata col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="item form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="<?php echo users_url('lists/departments'); ?>" class="btn btn-primary">Cancel</a>
                                <button id="formdata_confirm" type="button" class="btn btn-success" data-toggle="modal" href="#dfltmodal">Submit</button>
                            </div>
                        </div>
                    </form>
                </div><!-- end of .x_content -->
            </div><!-- end of .x_panel -->
        </div><!-- end of div -->
    </div><!-- end of .clearflash -->
</div><!-- end of .right_col -->
<script type="text/javascript">
    $(document).ready(function () {
        console.log(init_validator());
        $('#formdata_confirm').on('click', {
            'template': "<?php echo system_url("template/confirmation"); ?>",
            'action': "<?php echo departments_url("departments/method/add_department"); ?>",
            'message': "You are about to add a new department. Check if the details correct.",
            'redirect': "<?php echo departments_url('list'); ?>"
        }, load_dfltconfirmation);
    });
</script>   
<?php $this->load->view(system_dir('template/footer')); ?>
<?php

/* ------------------------------------------------------------------------------------- */
/* ------------------------------- Routes for Departments ------------------------------ */
/* ------------------------------------------------------------------------------------- */
$route['departments/list'] = "departments/list_departments";
$route['departments/view/(:any)'] = "departments/view_department/$1";
$route['departments/add'] = "departments/add_department";
$route['departments/edit/(:any)'] = "departments/edit_department/$1";

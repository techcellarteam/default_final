// document ready function
$(document).ready(function () {

});//End document ready functions
$(setInterval(function () {
    $('li.dropdown ul.notifications').load(systemURL + 'notifications/modules_notification'); //this means that the items loaded by display.php will be prompted into the class refresh 
$.ajax({
        url: systemURL + 'notifications/count_notifications'
    }).done(function (data) {
        $('span.notification').html(data);
        $('ul.notif li.header').append('('+data+') Items');
    });
}, 2000));

function update_seen(id) {
    //update notification as seen
    $.ajax({
        url: systemURL + 'notifications/method/update_notification',
        type: 'POST',
        data: {'id': id}
    }).done(function (data) {
        //check notification count
        $('span.notification').html(data);
    });
}